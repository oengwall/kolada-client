
import unittest
import kolada
import kolada.entity as entity
import kolada.data as data


kolada.use_cache(False)


class Basic(unittest.TestCase):

    def test_single_kpi(self):
        kpi_id = 'N00005'
        kpi = entity.Kpi(kpi_id)
        self.assertIsInstance(kpi._data, dict)

    def test_all_kpigroups(self):
        groups = entity.KpiGroups.all()
        self.assertIsInstance(groups, list)

    def test_data(self):
        kpi = entity.Kpi('N00005')
        m = entity.Municipality.all()
        years = entity.Year.range(1990, 2019)
        d = list(data.Data.get([kpi], m, years))
        self.assertGreater(len(d), 0)

    def test_search(self):
        needle = 'Antal invånare'
        kpis = entity.Kpi.search(needle)
        for kpi in kpis:
            for part in needle.split():
                self.assertIn(part.lower(), kpi.title.lower())
