from typing import List, Optional
import click
import kolada
import csv
import logging

kolada.use_cache(True)


log = logging.getLogger(__name__)


class BaseError(RuntimeError):
    pass


class ConfigError(BaseError):
    pass


class EntityNotFound(ConfigError):
    def __init__(self, entity):
        self._entity = entity
        super().__init__(f'Entity "{entity}" not found')


class Config:
    def __init__(self):
        self.entity = None
        self.groups = []
        self.representation = ['id', 'title']
        self.output = None
        self.logn = -1

    def lookup_entity(self):
        for name in dir(kolada.entity):
            log.debug('looking at %s for %s', name, self.entity)
            if name.lower() == self.entity:
                entity = getattr(kolada.entity, name)
                if issubclass(entity, kolada.entity.KoladaEntity):
                    log.debug('found: %s', entity.__name__)
                    return entity
        raise EntityNotFound(self.entity)

    def log(self, i):
        if self.logn > 0 and i % self.logn == 0:
            click.echo(f'outputting row no. {i}')


config = click.make_pass_decorator(Config, ensure=True)


# helper class

class Helper:
    @staticmethod
    def parse_list(slist: str):
        items = (it for it in slist.split(','))
        return list(it for it in items if it)

    @staticmethod
    def float_repr(val: Optional[float]):
        if val is not None:
            return repr(val)
        return ''

    def parse_group(self, group: str):
        return self.parse_list(group)

    def parse_representation(self, rep: str):
        return self.parse_list(rep)

    def iter_entity(self,
                    entity,
                    groups: Optional[List[str]] = None,
                    item_ids: Optional[List[str]] = None,
                    populate: bool = True):
        if item_ids:
            for item in item_ids:
                yield entity(item)

        elif groups:
            group_class = entity.group_class()
            seen = set()
            for gid in groups:
                group = group_class.single(gid)
                allmembers = None
                if populate:
                    allmembers = {mem.id: mem for mem in entity.all()}
                for mem in group.members(populate=populate, allmembers=allmembers):
                    if mem.id not in seen:
                        yield mem
                    seen.add(mem.id)
        else:
            yield from entity.all()

    def get_values(self, genders, data):
        return [self.float_repr(getattr(data, 'value_'+gidx)) for gidx in genders]


@click.group()
@click.option('--logn', default=-1, help='Log every n:th row to stdout')
@config
def root(config: Config, logn: int):
    config.logn = logn


@root.command()
@click.argument('command')
@config
def cache(config: Config, command):
    '''
    The cache command may be [clear|recreate]
    '''
    import kolada.litecache

    cache = kolada.litecache.LiteCache()
    if command == 'clear':
        cache.clear()
    elif command == 'recreate':
        cache.recreate_cache()


@root.command(name='list')
@click.option('-o', '--output', type=click.File('w'), default='-', help='ouputfile,  - is stdout')
@click.option('--group', default='', help='comma separated entity group(s) to be used')
@click.option('--rep', default='id,title', help='comma separated representation of the entity')
@click.argument('entity-name')
@config
def xlist(config: Config, output, group: str, rep: str, entity_name: str):
    helper = Helper()
    config.representation = helper.parse_representation(rep)
    config.groups = helper.parse_group(group)
    config.output = output
    config.entity = entity_name
    entity = config.lookup_entity()
    writer = csv.writer(config.output)
    for i, e in enumerate(helper.iter_entity(entity, config.groups)):
        writer.writerow([getattr(e, r) for r in config.representation])
        config.log(i)


@root.command()
@click.option('-o', '--output', type=click.File('w'), default='-', help='ouputfile,  - is stdout')
@click.option('--kpi', default='', help='a number of kpis')
@click.option('--kpigroup', default='', help='a number of kpi groups')
@click.option('--municipality', default='', help='a number of municipalites')
@click.option('--municipalitygroup', default='', help='a number of municipality groups')
@click.option('--year', default='', help='a number of years')
@click.option('--gender', default='t', help='number of genders to add (t, k, m)')
@config
def data(config, output, kpi, kpigroup, municipality, municipalitygroup, year, gender):
    helper = Helper()
    kpis = list(helper.iter_entity(kolada.entity.Kpi,
                                   item_ids=helper.parse_list(kpi),
                                   groups=helper.parse_list(kpigroup),
                                   populate=False))
    municipalities = list(helper.iter_entity(kolada.entity.Municipality,
                                             item_ids=helper.parse_list(municipality),
                                             groups=helper.parse_list(municipalitygroup),
                                             populate=False))
    years = list(helper.iter_entity(kolada.entity.Year, item_ids=helper.parse_list(year)))
    genders = list(helper.parse_list(gender))
    writer = csv.writer(output)
    for i, dp in enumerate(kolada.data.Data.get_iterator(kpis, municipalities, years)):
        vals = helper.get_values(genders, dp)
        writer.writerow([dp.kpi.id, dp.municipality.id, dp.year.id]+vals)
        config.log(i)
