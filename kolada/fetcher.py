
from typing import Optional, Iterator
import json
import urllib.request
import logging


class RawFetcher:
    def __init__(self, start_url: str):
        self.start_url = start_url
        self.current_url = start_url

    def fetch(self, url: str) -> Optional[dict]:
        if not url:
            return None
        logging.getLogger(__name__).info('url: %s', url)
        resp = urllib.request.urlopen(url)
        data = resp.read()
        return json.loads(data)

    @staticmethod
    def next(jdata:  dict) -> Optional[str]:
        return jdata.get('next_page', None)

    def iter_all_values(self) -> Iterator[dict]:
        url = self.start_url
        while True:
            data = self.fetch(url)
            if not data:
                break
            url = self.next(data)
            for val in data['values']:
                yield val


class CachingFetcher(RawFetcher):
    def __init__(self, start_url:  str):
        from .litecache import LiteCache

        super().__init__(start_url)
        self._cache = LiteCache()

    def fetch(self, url) -> Optional[dict]:
        val = self._cache.get(url)
        if val is not None:
            return val
        val = super().fetch(url)
        self._cache.put(url, val)
        return val


Fetcher = CachingFetcher


def use_cache(docache: bool = True):
    """
    Install a caching or non-caching fetch from kolada-api
    """
    fetcher = CachingFetcher if docache else RawFetcher
    globals()['Fetcher'] = fetcher


def fetchall(url: str):
    """
    Fetches the entire dataset starting at the given url
    """
    f = Fetcher(url)
    return list(f.iter_all_values())


def iterall(url: str):
    f = Fetcher(url)
    return f.iter_all_values()
