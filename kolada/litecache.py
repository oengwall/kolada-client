from typing import Optional
import sqlite3 as db
import pathlib
import json
import logging


class CacheInterface:
    def get(self, key: str):
        raise NotImplementedError()

    def put(self, key: str, val: dict):
        raise NotImplementedError()

    def delete(self, key: str):
        raise NotImplementedError()


class LiteCache(CacheInterface):
    def __init__(self, dbfile: Optional[str] = None):
        if dbfile is None:
            kolada_config_lib = pathlib.Path.home() / '.config' / 'kolada'
            kolada_config_lib.mkdir(parents=True, exist_ok=True)
            dbfile = kolada_config_lib / 'cache.db'
            dbfile = str(dbfile)
        logging.getLogger(__name__).info('dbfile: %s', dbfile)
        self.db = db.connect(dbfile)
        self.assert_db()

    def assert_db(self):
        sql = '''
create table if not exists kcache (
    k text primary key,
    v text not null,
    created date default current_timestamp
);

delete from kcache
where
    created < date('now')
;
'''
        with self.db:
            self.db.executescript(sql)

    def get(self, key: Optional[str]) -> Optional[dict]:
        if key is None:
            return None
        sql = '''
        select v
        from kcache
        where k = ?
        '''
        for row in self.db.execute(sql, (key,)):
            logging.getLogger(__name__).debug('hit: %s', key)
            return json.loads(row[0])
        logging.getLogger(__name__).debug('miss: %s', key)
        return None

    def put(self, key:  Optional[str], val: Optional[dict]):
        if key is not None and val is not None:
            valstr = json.dumps(val)
            sql = '''
            insert into kcache(k, v)
            values (?, ?)'''
            with self.db:
                self.db.execute(sql, (key, valstr))

    def delete(self, key: Optional[str]):
        if key is not None:
            sql = '''
            delete from kcache
            where k = ?'''
            with self.db:
                self.db.execute(sql, (key,))

    def clear(self):
        sql = '''
        delete from kcache
        '''
        with self.db:
            self.db.execute(sql)

    def recreate_cache(self):
        sql = '''
        drop table kcache
        '''
        with self.db:
            self.db.execute(sql)
            self.assert_db()
