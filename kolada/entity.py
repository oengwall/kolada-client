
import datetime
from typing import Union, Optional
import urllib.parse
from . import fetcher


def kolada_url(*pinfo, **queryparams):
    baseurl: str = 'http://api.kolada.se/v2/'
    parts = [baseurl]
    parts.append('/'.join(urllib.parse.quote(p) for p in pinfo))
    if queryparams:
        parts += ['?', urllib.parse.urlencode(queryparams)]
    return ''.join(parts)


class Year:
    id: str = ''
    startyear: int = 1990

    def __init__(self, id_: Union[int, str]):
        self.id = str(id_)

    @property
    def title(self):
        return self.id

    def __str__(self):
        return f'{self.id}'

    @classmethod
    def range(cls, start, end):
        return list(Year(y) for y in range(start, end))

    @classmethod
    def all(cls):
        thisyear = datetime.date.today().year
        return cls.range(cls.startyear, thisyear)


class KoladaEntity:
    id: str = ''
    entity_name: str = ''

    def __init__(self, id_or_data: Union[str, dict], assert_data: bool = True):
        if isinstance(id_or_data, str):
            self.id = id_or_data
            self._data = None
        else:
            self._data = id_or_data
            self.id = self._data['id']
        if assert_data:
            self.assert_data()

    def assert_data(self):
        if not self.id:
            raise ValueError('No id given')
        if not self._data:
            url = kolada_url(self.entity_name, self.id)
            self._data = fetcher.fetchall(url)[0]

    def __getattr__(self, attr):
        self.assert_data()
        try:
            return self._data[attr]
        except KeyError:
            raise AttributeError(self.__class__.__name__ + f' does not have an attribute {attr}')

    def __str__(self):
        return f'({self.id}) {self.title}'

    @classmethod
    def single(cls, id):
        url = kolada_url(cls.entity_name, id)
        rows = fetcher.fetchall(url)
        if rows:
            return cls(rows[0], False)
        return None

    @classmethod
    def all(cls):
        """
        Returns all entities of the classtype from kolada-api
        """
        url = kolada_url(cls.entity_name)
        return list(cls(row, False) for row in fetcher.fetchall(url))

    @classmethod
    def search(cls, title):
        """
        Returns all entities of the classtype matching the title
        """
        url = kolada_url(cls.entity_name, title=title)
        return list(cls(row, False) for row in fetcher.fetchall(url))

    @classmethod
    def group_class(cls):
        return None

    @classmethod
    def member_class(cls):
        return None


class Kpi(KoladaEntity):
    entity_name: str = 'kpi'

    @property
    def unit(self):
        """Adhoc way of finding "unit", assumes that unit is the last thing
        in the title after the last comma.
        """
        _, _, u = self.title.rpartition(',')
        return u

    @classmethod
    def group_class(cls):
        return KpiGroups


class KoladaEntityGroup(KoladaEntity):
    _members = None

    def iter_members(self, populate: bool = True, allmembers: Optional[dict] = None):
        # we assume that the population will be better if we just load
        # all members into a dictionary lookup cache
        MemberType = self.member_class()
        if populate:
            allmembers = allmembers or {mem.id: mem for mem in self.member_class().all()}
        for item in self._data['members']:
            if populate:
                # Apparently some members may not exist
                mem = allmembers.get(item['member_id'], None)
            else:
                mem = MemberType(item['member_id'])
            if mem is not None:
                yield mem

    def members(self, populate: bool = True, allmembers: Optional[dict] = None):
        return list(self.iter_members(allmembers))


class KpiGroups(KoladaEntityGroup):
    entity_name: str = 'kpi_groups'

    @classmethod
    def member_class(cls):
        return Kpi


class Municipality(KoladaEntity):
    entity_name: str = 'municipality'

    @classmethod
    def group_class(cls):
        return MunicipalityGroups


class MunicipalityGroups(KoladaEntityGroup):
    entity_name:  str = 'municipality_groups'

    @classmethod
    def member_class(cls):
        return Municipality


class Ou(KoladaEntity):
    entity_name: str = 'ou'
