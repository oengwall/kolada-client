
from typing import Sequence
import itertools
from .entity import kolada_url, Year, Municipality, Kpi
from .fetcher import iterall
from .errors import KoladaLookupError


def chunker(lst: Sequence, maxsize=100):
    ptr = 0
    result = []
    while True:
        result.append(lst[ptr:ptr+maxsize])
        ptr += maxsize
        if ptr >= len(lst):
            break
    return result


def commajoin(entities):
    return ','.join(e.id for e in entities)


class EntityLookup:
    def __init__(self, *entity_lists):
        self._lookup = {}
        for lst in entity_lists:
            for ent in lst:
                d = self._lookup.setdefault(ent.entity_name, {})
                d[ent.id] = ent

    def lookup(self, name, id_):
        try:
            return self._lookup[name][id_]
        except KeyError:
            raise KoladaLookupError('Entity: {}, id={}'.format(name, id_))

    def resolve_row(self, row):
        vals = {}
        try:
            rkpi = self.lookup('kpi', row['kpi'])
            rmuni = self.lookup('municipality', row['municipality'])
        except KoladaLookupError:
            return None
        ryear = Year(row['period'])
        for val in row.get('values', []):
            valname = 'value_'+val['gender'].lower()
            vals[valname] = val['value']
        return Data(rkpi, rmuni, ryear, **vals)


class Data:
    def __init__(self, kpi, municipality, year, value_t=None, value_k=None, value_m=None):
        self.kpi = kpi
        self.municipality = municipality
        self.year = year
        self.value_t = value_t
        self.value_k = value_k
        self.value_m = value_m

    def __str__(self):
        return f'{self.kpi}, {self.municipality}, {self.year}: {self.value}'

    @property
    def value(self):
        return self.value_t

    @classmethod
    def iter_by_kpi(cls, kpi, years):
        if isinstance(kpi, Kpi):
            kpi = [kpi]
        municipalities = Municipality.all()
        lookup = EntityLookup(kpi, municipalities)
        url = kolada_url('data', 'kpi', commajoin(kpi), 'year', commajoin(years))
        for row in iterall(url):
            row = lookup.resolve_row(row)
            if row is not None:
                yield row

    @classmethod
    def iter_by_municipality(self, municipality, years):
        if isinstance(municipality, Municipality):
            municipality = [municipality]
        kpis = Kpi.all()
        lookup = EntityLookup(kpis, municipality)
        url = kolada_url('data', 'municipality', commajoin(municipality), 'year', commajoin(years))
        for row in iterall(url):
            row = lookup.resolve_row(row)
            if row is not None:
                yield row

    @classmethod
    def get_iterator(cls, kpis, municipalities, years):
        lookup = EntityLookup(kpis, municipalities)
        kpis = chunker(kpis, 100)
        municipalities = chunker(municipalities, 100)
        years = chunker(years, 100)
        for k, m, y in itertools.product(kpis, municipalities, years):
            url = kolada_url('data', 'kpi', commajoin(k), 'municipality', commajoin(m), u'year', commajoin(y))
            for row in iterall(url):
                row = lookup.resolve_row(row)
                if row is not None:
                    yield row

    @classmethod
    def get(cls, kpis, municipalities, years):
        return list(cls.get_iterator(kpis, municipalities, years))
