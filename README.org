* Kolada python client

A simple kolada client for the api (v2) located at http://api.kolada.se/. 

* Installation

Installing is easiest done via pip and git support. E.g.

#+BEGIN_SRC sh
  pip install git+https://gitlab.com/oengwall/kolada-client@v0.01
#+END_SRC

This should probably be done in a virtual environment

Create the environment
#+BEGIN_SRC sh
python -m venv kolada
#+END_SRC

Activate it
#+BEGIN_SRC sh
# Linux/MAC bash
. kolada/bin/activate

# Windows cmd
kolada\Scripts\activate.bat

# Windows powershell
. kolada/Scripts/activate.ps1
#+END_SRC

Install in the virtual environment
#+BEGIN_SRC sh
pip install git+https://gitlab.com/oengwall/kolada-client@v0.02
#+END_SRC

* Usage
There are some usage examples in the test-folder, but the basic use-pattern is
#+BEGIN_SRC python
import kolada
kpi = kolada.entity.Kpi('N00005')
years = kolada.entity.Years.range(2015, 2020)
municipalities = kolada.entity.Municipalites.all()
data = kolada.data.Data.get([kpi], municipalites, years)
#+END_SRC


* Commandline utility
The module defines a commandline utility, kolada, that may be used to
retrive csv-data from the API. There are some minor help-description
given with --help. I.e.

#+BEGIN_SRC sh
prompt:/tmp$ kolada --help
Usage: kolada [OPTIONS] COMMAND [ARGS]...

Options:
  --logn INTEGER  Log every n:th row to stdout
  --help          Show this message and exit.

Commands:
  cache  The cache command may be [clear|recreate]
  data
  list
#+END_SRC

Further help is given by the commands themselfs, i.e.
#+BEGIN_SRC sh
prompt:/tmp$ kolada list --help
Usage: kolada list [OPTIONS] ENTITY_NAME

Options:
  -o, --output FILENAME  ouputfile,  - is stdout
  --group TEXT           comma separated entity group(s) to be used
  --rep TEXT             comma separated representation of the entity
  --help                 Show this message and exit. 
#+END_SRC 
The list-command is for listing entities, the data-command is for
retrieving data and the cache command is for the cache that the
kolada-client is using.

** Examples

List all kpis
#+BEGIN_SRC sh
kolada list kpi
#+END_SRC

This will list all kpis in kolada, in a id,title representation. The
following entities may be listed in the same way:
  - kpi
  - kpigroups
  - municipality
  - municipalitygroups

The representation (rep) maybe set to any of the defined attributes by
the api (see https://github.com/Hypergene/kolada). One attribute was
added to the kpi, which is unit (very experimental). And so we may be
adding that by using the optional rep-parameter.

#+BEGIN_SRC sh
kolada list kpi --rep="id,title,unit"
#+END_SRC


Data may be retrived with the data-command. This command currently be
default tries to download all data, which is probably not what you
want. So we need to set some limitations. From the help we get

#+BEGIN_SRC sh
promt:/tmp$ kolada data --help
Usage: kolada data [OPTIONS]

Options:
  -o, --output FILENAME     ouputfile,  - is stdout
  --kpi TEXT                a number of kpis
  --kpigroup TEXT           a number of kpi groups
  --municipality TEXT       a number of municipalites
  --municipalitygroup TEXT  a number of municipality groups
  --year TEXT               a number of years
  --gender TEXT             number of genders to add (t, k, m)
  --help                    Show this message and exit.
#+END_SRC

So id we want to download all kpis for a specific municipality for instance, we
#+BEGIN_SRC sh
kolada data --municipality=2513
#+END_SRC

Or the same for the entire an entire group
#+BEGIN_SRC sh
kolada data --municipalitygroup=G92794
#+END_SRC

The same applies for kpi and kpigroups. By default only the "total"
value is persented, but you may set that by adding the gender option. 
#+BEGIN_SRC sh
kolada data --kpigroup=GKPI97 --gender=t,k,m
#+END_SRC

For all commands you may give an output-file (-o) and if you retrieve
really large datasets you may want to set the --logn parameter if you
using a file as output.
