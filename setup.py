import os
from setuptools import setup, find_packages

deps = [
    'Click'
    ]


README = open(os.path.join(os.path.dirname(__file__), 'README.org')).read()


setup(name='kolada',
      version='0.1-alpha',
      author='Ola Engwall',
      author_email='ola.engwall@gmail.com',
      packages=find_packages(),
      license='MIT License',
      long_description=README,
      include_package_data=True,
      url='https://gitlab.com/oengwall/kolada-client/',
      install_requires=deps,
      entry_points='''
      [console_scripts]
      kolada = kolada.cli.cli:root
      ''')
