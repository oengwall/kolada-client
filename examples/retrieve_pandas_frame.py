import kolada
import pandas


def iter_data(kpis, years=None):
    if years is None:
        years = kolada.entity.Year.range(1990, 2018)
    yield from kolada.data.Data.iter_by_kpi(kpis, years)


def get_frame():
    kpis_ids = ['N00009', 'N00005']
    kpis = [kolada.entity.Kpi(kid) for kid in kpis_ids]
    data = {
        'kpi': [],
        'municipality': [],
        'year': [],
        'value': []
    }
    for row in iter_data(kpis):
        data['year'].append(row.year.id)
        data['municipality'].append(row.municipality.title)
        data['kpi'].append(row.kpi.title)
        data['value'].append(row.value_t)
    pframe = pandas.DataFrame(data)
    kpitable = pframe.pivot_table(index=['year', 'municipality'], columns='kpi', values='value')
    return kpitable


def example():
    pf = get_frame()
    print(pf)


example()
